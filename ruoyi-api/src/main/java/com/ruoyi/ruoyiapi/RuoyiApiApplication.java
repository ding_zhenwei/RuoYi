package com.ruoyi.ruoyiapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuoyiApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuoyiApiApplication.class, args);
    }

}
